$('#nav-toggler').click(function () {
  $(this).toggleClass('open');
  $('.navbar-collapse').toggleClass('show');
});

$('.navbar-nav .nav-item .nav-link').click(function () {
  $('.navbar-collapse').removeClass('show');
  $('#nav-toggler').removeClass('open');
});

$('#go_to_contacts').click(function () {
  $('.modal').hide();
  $('.modal-backdrop').hide();
});

$('.go_to_contacts').click(function () {
  $('.modal').hide();
  $('.modal-backdrop').hide();
});

$('a').click(function () {
  $('html, body').animate({
    scrollTop: $($(this).attr('href')).offset().top - 100
  }, 500);
  return false;
});

$(".close-btn, .bg-overlay").click(function () {
  $(".custom-model-main").removeClass('model-open');
});

// Cache selectors
var topMenu = $(".navbar-nav"),
  topMenuHeight = topMenu.outerHeight() + 15,
  // All list items
  menuItems = topMenu.find("a"),
  // Anchors corresponding to menu items
  scrollItems = menuItems.map(function () {
    var item = $($(this).attr("href"));
    if (item.length) { return item; }
  });

// Bind to scroll
$(window).scroll(function () {
  // Get container scroll position
  var fromTop = $(this).scrollTop() + topMenuHeight;

  // Get id of current scroll item
  var cur = scrollItems.map(function () {
    if ($(this).offset().top - 120 < fromTop)
      return this;
  });
  // Get the id of the current element
  cur = cur[cur.length - 1];
  var id = cur && cur.length ? cur[0].id : "";
  // Set/remove active class
  menuItems
    .parent().removeClass("active")
    .end().filter("[href='#" + id + "']").parent().addClass("active");
});

const yearWrappers = document.querySelectorAll('#current-year')
if (yearWrappers) {
  yearWrappers.forEach(wrapper => wrapper.innerText = new Date().getFullYear())
}

//CountUp

