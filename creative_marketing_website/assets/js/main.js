$('#nav-toggler').click(function () {
  $(this).toggleClass('open');
  $('.navbar-collapse').toggleClass('show');

  if ($(".navbar-collapse").hasClass("show")) {
    $(".navbar").addClass('bg-toggler');
  } else {
    $(".navbar").removeClass('bg-toggler');
  }
});

$('.navbar-nav .nav-item .nav-link').click(function () {
  $('.navbar-collapse').removeClass('show');
  $('#nav-toggler').removeClass('open');
});

$('#go_to_contacts').click(function () {
  $('.modal').hide();
  $('.modal-backdrop').hide();
});

$('.go_to_contacts').click(function () {
  $('.modal').hide();
  $('.modal-backdrop').hide();
});

$('a').click(function () {
  $('html, body').animate({
    scrollTop: $($(this).attr('href')).offset().top - 100
  }, 500);
  return false;
});

$(".close-btn, .bg-overlay").click(function () {
  $(".custom-model-main").removeClass('model-open');
});

// Cache selectors
var topMenu = $(".navbar-nav"),
  topMenuHeight = topMenu.outerHeight() + 15,
  // All list items
  menuItems = topMenu.find("a"),
  // Anchors corresponding to menu items
  scrollItems = menuItems.map(function () {
    var item = $($(this).attr("href"));
    if (item.length) { return item; }
  });

// Bind to scroll
$(window).scroll(function () {
  // Get container scroll position
  var fromTop = $(this).scrollTop() + topMenuHeight;

  // Get id of current scroll item
  var cur = scrollItems.map(function () {
    if ($(this).offset().top - 120 < fromTop)
      return this;
  });
  // Get the id of the current element
  cur = cur[cur.length - 1];
  var id = cur && cur.length ? cur[0].id : "";
  // Set/remove active class
  menuItems
    .parent().removeClass("active")
    .end().filter("[href='#" + id + "']").parent().addClass("active");
});

$(window).scroll(function () {
  var oTop = $('#counter').offset().top - window.innerHeight;
  if ($(window).scrollTop() > oTop) {
    $('.counter-value-slow').each(function () {
      var $this = $(this),
        countTo = $this.attr('data-count');
      $({
        countNum: $this.text()
      }).animate({
        countNum: countTo
      },

        {

          duration: 2500,
          easing: 'swing',
          step: function () {
            $this.text(Math.floor(this.countNum));
          },
          complete: function () {
            $this.text(this.countNum);
            //alert('finished');
          }
        });
    });
    $('.counter-value-faster').each(function () {
      var $this = $(this),
        countTo = $this.attr('data-count');
      $({
        countNum: $this.text()
      }).animate({
        countNum: countTo
      },

        {

          duration: 1000,
          easing: 'swing',
          step: function () {
            $this.text(Math.floor(this.countNum));
          },
          complete: function () {
            $this.text(this.countNum);
            //alert('finished');
          }
        });
    });
    a = 1;
  }
});

$(document).on('scroll', function () {
  if ($(window).scrollTop() > 10) {
    $(".navbar").addClass("nav-bg");
  }
  else {
    $(".navbar").removeClass("nav-bg");
  }

  if ($(window).scrollTop() >= $('#what_we_do').offset().top - 200) {
    $('#what_we_do .title-section').addClass('title-animated');
  } else {
    $('#what_we_do .title-section').removeClass('title-animated');
  }

  if ($(window).scrollTop() >= $('#our_story').offset().top - 200) {
    $('#our_story .title-section').addClass('title-animated');
    $('#our_story .image').addClass('animated-image');

  } else {
    $('#our_story .title-section').removeClass('title-animated');
    $('#our_story .image').removeClass('animated-image');
  }

  if ($(window).scrollTop() >= $('#promise').offset().top - 200) {
    $('#promise .title-section').addClass('title-animated');
  } else {
    $('#promise .title-section').removeClass('title-animated');
  }

  if ($(window).scrollTop() >= $('#about_us').offset().top - 200) {
    $('#about_us .title-section').addClass('title-animated');
    $('#about_us .image').addClass('animated-image');
  } else {
    $('#about_us .title-section').removeClass('title-animated');
    $('#about_us .image').removeClass('animated-image');
  }
  if ($(window).scrollTop() >= $('#contact_us').offset().top - 200) {
    $('#contact_us .title-section').addClass('title-animated');
    $('#contact_us .image').addClass('animated-image');
  } else {
    $('#contact_us .title-section').removeClass('title-animated');
    $('#contact_us .image').removeClass('animated-image');
  }
});

const yearWrappers = document.querySelectorAll('#current-year')
if (yearWrappers) {
  yearWrappers.forEach(wrapper => wrapper.innerText = new Date().getFullYear())
}
