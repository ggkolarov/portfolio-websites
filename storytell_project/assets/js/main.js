$('#nav-toggler').click(function () {
  $(this).toggleClass('open');
  $('.navbar-collapse').toggleClass('show');

  if ($(".navbar-collapse").hasClass("show")) {
    $(".navbar").addClass('bg-toggler');
  } else {
    $(".navbar").removeClass('bg-toggler');
  }
});

$('.navbar-nav .nav-item .nav-link').click(function () {
  $('.navbar-collapse').removeClass('show');
  $('#nav-toggler').removeClass('open');
});

$('#go_to_contacts').click(function () {
  $('.modal').hide();
  $('.modal-backdrop').hide();
});

$('.go_to_contacts').click(function () {
  $('.modal').hide();
  $('.modal-backdrop').hide();
});

$('a').click(function () {
  $('html, body').animate({
    scrollTop: $($(this).attr('href')).offset().top - 100
  }, 500);
  return false;
});

$(".close-btn, .bg-overlay").click(function () {
  $(".custom-model-main").removeClass('model-open');
});

// Cache selectors
var topMenu = $(".navbar-nav"),
  topMenuHeight = topMenu.outerHeight() + 15,
  // All list items
  menuItems = topMenu.find("a"),
  // Anchors corresponding to menu items
  scrollItems = menuItems.map(function () {
    var item = $($(this).attr("href"));
    if (item.length) { return item; }
  });

// Bind to scroll
$(window).scroll(function () {
  // Get container scroll position
  var fromTop = $(this).scrollTop() + topMenuHeight;

  // Get id of current scroll item
  var cur = scrollItems.map(function () {
    if ($(this).offset().top - 120 < fromTop)
      return this;
  });
  // Get the id of the current element
  cur = cur[cur.length - 1];
  var id = cur && cur.length ? cur[0].id : "";
  // Set/remove active class
  menuItems
    .parent().removeClass("active")
    .end().filter("[href='#" + id + "']").parent().addClass("active");
});

$("#promise .row-box").hover(function () {
  $("#promise .row-box").removeClass('box-glass');
  $(this).addClass('box-glass');
});

$(document).on('scroll', function () {
  if ($(this).scrollTop() >= $('#hero .title').offset().top + 20) {
    $('#hero .box-glass').addClass('animation');
  } else {
    $('#hero .box-glass').removeClass('animation');
  }

  if ($(this).scrollTop() >= $('#about_us').offset().top - 200) {
    $('#about_us .crown').addClass('animation');
  } else {
    $('#about_us .crown').removeClass('animation');
  }

  if ($(this).scrollTop() >= $('#contact_us').offset().top - 100) {
    $('#contact_us .bell').addClass('animation');
  } else {
    $('#contact_us .bell').removeClass('animation');
  }
});

const yearWrappers = document.querySelectorAll('#current-year')
if (yearWrappers) {
  yearWrappers.forEach(wrapper => wrapper.innerText = new Date().getFullYear())
}

// Testimonials loop

$(document).ready(function() {
  		
  //Elements to loop through
  var elem1 = $('.testimonial1');
  var elem2 = $('.map-dot');
  //Start at 0
  i = 0;
  
  function getMessage() {
    
    //Loop through elements
    $(elem1).each( function(index) {
      
      if ( i == index ) {
        //Show active element
        $(this).show();
      } else if ( i == $(elem1).length ) {
        //Show message
        $(this).show();
        //Reset i lst number is reached
        i = 0;
      } else {
        //Hide all non active elements
        $(this).hide();
      }
      
    });
    
    //Loop through elements
    $(elem2).each( function(index) {
      
      if ( i == index ) {
        //Show active element
        $(this).show();
      } else if ( i == $(elem2).length ) {
        //Show message
        $(this).show();
        //Reset i lst number is reached
        i = 0;
      } else {
        //Hide all non active elements
        $(this).hide();
      }
      
    });
    
    i++;
  
  }
  
  //Run once the first time
  getMessage();
  
  //Repeat
  window.setInterval(getMessage, 6000);
  
});