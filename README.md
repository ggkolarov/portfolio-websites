# Portfolio websites


Websites were created using the following technologies:
HTML5
SASS
CSS
Bootstrap
JavaScript
jQuery
PHP

I prefer to write custom HTML code without any HTML frameworks. PHP code is using for the contact form which sends and email via Email. 

In almost each website is using animation, some of the animations were created custom with CSS and jQuery and some of them were created using AOS animation framework. 

All of the websites are fully responsive. It is used media query for that.

