$(document).ready(function(){   
  $('#nav-toggler').click(function(){
    $(this).toggleClass('open');
    $('.navbar-collapse').toggleClass('show');
  });

  $('.navbar-nav .nav-item .nav-link').click(function(){
    $('.navbar-collapse').removeClass('show');
    $('#nav-toggler').removeClass('open');
  });

  $('#go_to_contacts').click(function(){
    $('.modal').hide();
    $('.modal-backdrop').hide();
  });

  $('.go_to_contacts').click(function(){
    $('.modal').hide();
    $('.modal-backdrop').hide();
  })

  $(window).on('scroll', function() {
     if($(window).scrollTop() > $('#header').height() || $(window).width() < 992){
        $('.navbar').removeClass('vertical-menu', 1000);
     } else{
      $('.navbar').addClass('vertical-menu');
     }
  });

  if($(window).width() < 992){
    $('.navbar').removeClass('vertical-menu');
 }

});

$(document).on('scroll', function() {
  if($(this).scrollTop()>=$('#clients .image-container').offset().top - 200){
      $('#clients .image-container').addClass('animation-clients');
  } else{
    $('#clients .image-container').removeClass('animation-clients');
  }

  if($(this).scrollTop()>=$('#about_us .image-container').offset().top - 300){
      $('#about_us .image-container').addClass('animation-about-us');
  } else{
    $('#about_us .image-container').removeClass('animation-about-us');
  }

  if($(this).scrollTop()>=$('#boxes').offset().top - 200){
    $('#boxes .container-boxes').addClass('animation-boxes');
    $('#boxes').addClass('animation-lines');
  } else{
    $('#boxes .container-boxes').removeClass('animation-boxes');
    $('#boxes').removeClass('animation-lines');
  }

  if($(this).scrollTop()>=$('#marketing').offset().top - 100){
    $('#marketing').addClass('animation-marketing');
  } else{
    $('#marketing').removeClass('animation-marketing');
  }

  if($(this).scrollTop()>=$('#writing').offset().top - 100){
    $('#writing').addClass('animation-writing');
  } else{
    $('#writing').removeClass('animation-writing');
  }

  if($(this).scrollTop()>=$('#seo').offset().top - 100){
    $('#seo').addClass('animation-seo');
  } else{
    $('#seo').removeClass('animation-seo');
  }

  if($(this).scrollTop()>=$('#web').offset().top - 100){
    $('#web').addClass('animation-web');
  } else{
    $('#web').removeClass('animation-web');
  }

  if($(this).scrollTop()>=$('#hr-service').offset().top - 100){
    $('#hr-service').addClass('animation-hr');
  } else{
    $('#hr-service').removeClass('animation-hr');
  }

  if($(this).scrollTop()>=$('#financial').offset().top - 100){
    $('#financial').addClass('animation-financial');
  } else{
    $('#financial').removeClass('animation-financial');
  }

  if($(this).scrollTop()>=$('#transformation .image-container').offset().top - 400){
    $('#transformation .image-container').addClass('animation-transformation');
  } else{
    $('#transformation .image-container').removeClass('animation-transformation');
  }

  if($(this).scrollTop()>=$('.contact-form').offset().top - 300){
    $('.contact-form').addClass('animation-contact-us');
  } else{
    $('.contact-form').removeClass('animation-contact-us');
  }

  if($(this).scrollTop()>=$('#careers .image-container').offset().top - 300){
    $('#careers .image-container').addClass('animation-career-circles');
  } else{
    $('#careers .image-container').removeClass('animation-career-circles');
  }

})

$(document).ready(function () {
  $(document).on("scroll", onScroll);
  
  //smoothscroll
  $('a[href^="#"]').on('click', function (e) {
      e.preventDefault();
      $(document).off("scroll");
      
      $('a').each(function () {
          $(this).removeClass('active');
      })
      $(this).addClass('active');
    
      var target = this.hash,
          menu = target;
      $target = $(target);
      $('html, body').stop().animate({
          'scrollTop': $target.offset().top+2 
      }, 500, 'swing', function () {
          window.location.hash = target;
          $(document).on("scroll", onScroll);
      });
  });
});

function onScroll(event){
  var scrollPos = $(document).scrollTop();
  $('.navbar-expand-lg .navbar-nav .nav-link').each(function () {
      var currLink = $(this);
      var refElement = $(currLink.attr("href"));
      if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
          $('.navbar-expand-lg .navbar-nav .nav-link').removeClass("active");
          currLink.addClass("active");
      }
  });
}